#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <endian.h>

#define MSG_SIZE 64

typedef unsigned char BYTE;

void printMessage(const BYTE *ptr) {
  int i;
  uint32_t counter;
  uint64_t timestamp;

  for (i = 0; i < MSG_SIZE; i++) {
    if (i % 8 == 0) printf(" ");
    printf("%.2x", ptr[i]);
  }

  counter = be32toh(*(uint32_t*)(ptr+60));
  timestamp = be64toh(*(uint64_t*)(ptr+52)) & 0x0000ffffffffffff;

  printf(" (%u, %lu)\n", counter, timestamp);
}

void printBuffer() {
  int i;
  BYTE buf[MSG_SIZE];
  int fd  = open("buffer.dump", O_RDONLY);
  //struct stat fstats;

  //fstat(fd, &fstats);
  //fstats.st_size
  lseek(fd, 66368, SEEK_SET);

  for (i = 0; i < 64000; i += MSG_SIZE) {
    if (MSG_SIZE != read(fd, &buf, MSG_SIZE)) break;
    printMessage(buf);
  }
  close(fd);
}

int main(int argc, char *argv[]) {
  printBuffer();
  return 0;
}
