#!/usr/bin/python3

import struct
import zlib

# hexdump -C -n 4 -s 0x0000 (*4) FILE

size = 2**32
buf = bytearray(size * 4)

for input in range(size):
    input_data = struct.pack('>L', input)
    crc = zlib.crc32(input_data)
    output_data = struct.pack('>L', crc)
    start = crc * 4
    end = start + 4
    buf[start:end] = input_data
    if (not (input % 1000000)): print('%0.2f %%' % (input * 100 / size));


with open('reverse_crc.dat.new', 'wb') as f:
    f.write(buf)
