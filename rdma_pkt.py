#!/usr/bin/python3

import struct
import zlib
from scapy.all import sendp

def getEH(dst, src, type):
    header = b''
    header += bytes.fromhex(dst.replace(':',''))
    header += bytes.fromhex(src.replace(':',''))
    # header += struct.pack('!Q',dst)[2:]
    # header += struct.pack('!Q',src)[2:]
    header += struct.pack('!H', type)
    return header

def getGRH(ver, tc, fl, plen, nh, hl, sgid, dgid):
    header = b''
    header += struct.pack('!L', (ver << 28) + (tc << 20) + fl)
    header += struct.pack('!H', plen)
    header += struct.pack('B', nh)
    header += struct.pack('B', hl)
    header += bytes.fromhex(sgid.replace(':',''))
    header += bytes.fromhex(dgid.replace(':',''))
    return header

def getBTH(op, se, m, padcnt, tver, p_key, destqp, a, psn):
    header = b''
    header += struct.pack('B', op)
    header += struct.pack('B', (se << 7) + (m << 6) + (padcnt << 4) + tver)
    header += struct.pack('!H', p_key)
    header += b'\x00'
    header += struct.pack('!L',destqp)[1:]
    header += struct.pack('B', (a << 7))
    header += struct.pack('!L',psn)[1:]
    return header

def getRETH(va, rkey, dmalen):
    header = b''
    header += struct.pack('!Q', va)
    header += struct.pack('!L', rkey)
    header += struct.pack('!L', dmalen)
    return header

def getCRCData(pkt):
    data = b'\xff' * 8
    data += struct.pack('B', pkt[14] | 0x0f)
    data += b'\xff' * 3
    data += pkt[18:21]
    data += b'\xff'
    data += pkt[22:58]
    data += b'\xff'
    data += pkt[59:]
    return data;

def getICRC(data):
    return zlib.crc32(data)

def getSeed(p_crc):
    with open('reverse_crc.dat', 'rb') as f:
        f.seek(p_crc * 4)
        seed_data = f.read(4)
    return seed_data

# def getICRC(pkt):
#     data = b'\xff' * 8
#     data += struct.pack('B', pkt[14] | 0x0f)
#     data += b'\xff' * 3
#     data += pkt[18:21]
#     data += b'\xff'
#     data += pkt[22:58]
#     data += b'\xff'
#     data += pkt[59:]
#     p_crc = zlib.crc32(data[:60])
#     crc = zlib.crc32(data)
#     crc_le = struct.pack('<L', crc)
#     with open('reverse_crc.dat', 'rb') as f:
#         f.seek(p_crc * 4)
#         sub_data = f.read(4)
#
#     print('Complete CRC: 0x' + struct.pack('>L', crc).hex())
#     print('   CRC Bytes: %s %s' % (data[:60].hex(), data[60:].hex()))
#     print('    CRC (LE): 0x' + crc_le.hex())
#     return crc_le



# Payload
msg = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!!'
#msg = '?' * 64
payload = msg
while  (len(payload) % 4): payload += '\x00'


# Ethernet
dst_mac = '98:03:9b:98:ac:46'
src_mac = '02:00:00:00:00:01'
ethertype = 0x8915

# GRH
IPVer = 6
traffic_class = 0
flow_label = 0
payload_len = 32 + len(payload)
next_header = 27
hop_limit = 64
sgid = '00:00:00:00:00:00:00:00:00:00:ff:ff:0a:00:00:01'
dgid = '00:00:00:00:00:00:00:00:00:00:ff:ff:0a:00:00:01'

# BTH
# OpCode = 0x2a # For UC
OpCode = 0x0a # For RC
SE = 0
M = 1
PadCnt = len(payload) - len(msg)
TVer = 0x0
P_Key = 0xffff
DestQP = 0x117
A = 0
PSN = 0x000000

# RETH
VA = 0x7f43c231c000
R_Key = 0x1679
DMALen = len(msg)

pkt = b''
pkt += getEH(dst_mac, src_mac, ethertype);
pkt += getGRH(IPVer, traffic_class, flow_label, payload_len, next_header, hop_limit, sgid, dgid)
pkt += getBTH(OpCode, SE, M, PadCnt, TVer, P_Key, DestQP, A, PSN)
pkt += getRETH(VA, R_Key, DMALen)
pkt += bytes(payload, 'utf-8')

data = getCRCData(pkt)
crc = getICRC(data)
pkt += struct.pack('<L', crc)
# p_crc = getICRC(data[:60]) # For UC
p_crc = getICRC(data[:56]) # For RC
seed_data = getSeed(p_crc);

print('   CRC Bytes: %s %s' % (data[:60].hex(), data[60:].hex()))
print('Complete CRC: 0x' + struct.pack('>L', crc).hex())
print('    CRC (LE): 0x' + struct.pack('<L', crc).hex())
print(' Partial CRC: 0x' + struct.pack('>L', p_crc).hex())
print('  Substitute: 0x' + seed_data.hex())

cmd = '        Rule: bfrt.rdma.pipe.SwitchIngress.rdma_session.set_default_with_create_roce_pkt(%#x, %#x, %#x, %#x, %#x, 0x%s)'
print(cmd % (int(sgid.replace(':', ''), 16), int(dgid.replace(':', ''), 16), DestQP, VA, R_Key, seed_data.hex()))
#print('Packet: ', pkt)
#sendp(pkt, iface='enp59s0f1')
